Feature: HeatClinic Complete Purchase Transaction
    # As a GQP Systems Performance Engineer
    # I want to run a Cucumber/Gherkin BDD Test Scenario
    # In order to simulate user shopping activity

  @HeatClinic-Checkout
  Scenario Outline: Complete purchase transaction with chosen browser on GQP HeatClinic Demo Site (<hiptest-uid>)
    Given a user navigates to the HeatClinic Website at "<HeatClinicUrl>" with the "<BrowserName>" browser
    When the user logs in with username "<Username>" and password "<Password>"
    And the user adds Hoppin' Hot Sauce to the shopping cart
    And the user opens the shopping cart and initiates checkout
    And the user enters their billing and shipping information
    And the user selects fulfillment and shipping options
    And the user completes the checkout process
    Then the user should receive a message indicating that the order status is "<OrderStatus>"

    Examples:
      | HeatClinicUrl | BrowserName | Username | Password | OrderStatus | hiptest-uid |
      | http://localhost:9443/gqpartners | Firefox | someguy2@gmail.com | SomeGuy2 | Success! |  |

  @HeatClinic-AddRemove
  Scenario Outline: Add/Remove item(s) from cart with chosen browser from GQP HeatClinic Demo Site (<hiptest-uid>)
    Given a user navigates to the HeatClinic Website at "<HeatClinicUrl>" with the "<BrowserName>" browser
    When the user logs in with username "<Username>" and password "<Password>"
    And the user adds "<Item>" to the shopping cart
    And the user validates that "<ItemCountAdd>" item(s) was/were added to the shopping cart
    And the user removes "<ItemCountRemove>" item(s) from the shopping cart
    Then the count of items in the shopping car is equal to "<RemainingItemCount>"

    Examples:
      | HeatClinicUrl | BrowserName | Username | Password | Item | ItemCountAdd | ItemCountRemove | RemainingItemCount | hiptest-uid |
      | http://localhost:9443/gqpartners | Firefox | someguy2@gmail.com | SomeGuy2 | Dead Scotch Bonnet Hot Sauce | 1 | 1 | 0 |  |
