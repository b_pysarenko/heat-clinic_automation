package com.bakery;

import net.thucydides.core.annotations.Step;

public class Actionwords {


    @Step("<Enter Step Description>")
    public void aUserNavigatesToTheHeatClinicWebsiteAtP1WithTheP2Browser(String p1, String p2) {

    }

    @Step("<Enter Step Description>")
    public void theUserLogsInWithUsernameP1AndPasswordP2(String p1, String p2) {

    }

    @Step("<Enter Step Description>")
    public void theUserAddsHoppinHotSauceToTheShoppingCart() {

    }

    @Step("<Enter Step Description>")
    public void theUserOpensTheShoppingCartAndInitiatesCheckout() {

    }

    @Step("<Enter Step Description>")
    public void theUserEntersTheirBillingAndShippingInformation() {

    }

    @Step("<Enter Step Description>")
    public void theUserSelectsFulfillmentAndShippingOptions() {

    }

    @Step("<Enter Step Description>")
    public void theUserCompletesTheCheckoutProcess() {

    }

    @Step("<Enter Step Description>")
    public void theUserShouldReceiveAMessageIndicatingThatTheOrderStatusIsP1(String p1) {

    }

    @Step("<Enter Step Description>")
    public void theUserAddsDeadScotchBonnetHotSauceToTheShoppingCart() {

    }

    @Step("<Enter Step Description>")
    public void theUserValidatesThatP1ItemsWaswereAddedToTheShoppingCart(int p1) {

    }

    @Step("<Enter Step Description>")
    public void theUserRemovesP1ItemsFromTheShoppingCart(int p1) {

    }

    @Step("<Enter Step Description>")
    public void theCountOfItemsInTheShoppingCarIsEqualToP1(int p1) {

    }

    @Step("<Enter Step Description>")
    public void theUserAddsP1ToTheShoppingCart(String p1) {

    }
}
