package com.bakery;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class StepDefinitions {

    @Steps
    public Actionwords actionwords = new Actionwords();

    @Given("^a user navigates to the HeatClinic Website at \"(.*)\" with the \"(.*)\" browser$")
    public void aUserNavigatesToTheHeatClinicWebsiteAtP1WithTheP2Browser(String p1, String p2) {
        actionwords.aUserNavigatesToTheHeatClinicWebsiteAtP1WithTheP2Browser(p1, p2);
    }

    @When("^the user logs in with username \"(.*)\" and password \"(.*)\"$")
    public void theUserLogsInWithUsernameP1AndPasswordP2(String p1, String p2) {
        actionwords.theUserLogsInWithUsernameP1AndPasswordP2(p1, p2);
    }

    @When("^the user adds Hoppin' Hot Sauce to the shopping cart$")
    public void theUserAddsHoppinHotSauceToTheShoppingCart() {
        actionwords.theUserAddsHoppinHotSauceToTheShoppingCart();
    }

    @When("^the user opens the shopping cart and initiates checkout$")
    public void theUserOpensTheShoppingCartAndInitiatesCheckout() {
        actionwords.theUserOpensTheShoppingCartAndInitiatesCheckout();
    }

    @When("^the user enters their billing and shipping information$")
    public void theUserEntersTheirBillingAndShippingInformation() {
        actionwords.theUserEntersTheirBillingAndShippingInformation();
    }

    @When("^the user selects fulfillment and shipping options$")
    public void theUserSelectsFulfillmentAndShippingOptions() {
        actionwords.theUserSelectsFulfillmentAndShippingOptions();
    }

    @When("^the user completes the checkout process$")
    public void theUserCompletesTheCheckoutProcess() {
        actionwords.theUserCompletesTheCheckoutProcess();
    }

    @Then("^the user should receive a message indicating that the order status is \"(.*)\"$")
    public void theUserShouldReceiveAMessageIndicatingThatTheOrderStatusIsP1(String p1) {
        actionwords.theUserShouldReceiveAMessageIndicatingThatTheOrderStatusIsP1(p1);
    }


    @When("^the user validates that \"(.*)\" item\\(s\\) was/were added to the shopping cart$")
    public void theUserValidatesThatP1ItemsWaswereAddedToTheShoppingCart(int p1) {
        actionwords.theUserValidatesThatP1ItemsWaswereAddedToTheShoppingCart(p1);
    }

    @When("^the user removes \"(.*)\" item\\(s\\) from the shopping cart$")
    public void theUserRemovesP1ItemsFromTheShoppingCart(int p1) {
        actionwords.theUserRemovesP1ItemsFromTheShoppingCart(p1);
    }

    @Then("^the count of items in the shopping car is equal to \"(.*)\"$")
    public void theCountOfItemsInTheShoppingCarIsEqualToP1(int p1) {
        actionwords.theCountOfItemsInTheShoppingCarIsEqualToP1(p1);
    }

    @When("^the user adds \"(.*)\" to the shopping cart$")
    public void theUserAddsP1ToTheShoppingCart(String p1) {
        actionwords.theUserAddsP1ToTheShoppingCart(p1);
    }
}
